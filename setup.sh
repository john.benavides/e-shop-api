docker build -t symfony/php ./docker
docker-compose -f ./docker/docker-compose.yml up -d
docker container exec -t -w /symfony symfony ./setup-doctrine.sh