# E-Shop API
## Origine du projet
Ce projet est un test technique impliquant la création d'une API simplifiée pour une boutique.
Il est séparé en deux parties, le backend et le frontend.

Le backend est simple, il sert à créer, modifer et supprimer les produits que les utilisateurs pourront acheter.

Le frontend contient la majorité des actions possibles. Il est basé sur les users stories suivantes :
- Visualiser tous les produits existants
- Ajouter une quantité choisie d'un produit dans son panier
- Retirer tous les exemplaires d'un produit de son panier
- Modifier la quantité d'un produit dans son panier
- Obtenir une facture pour régler son panier
- Régler la facture du panier
- Abandonner le panier et supprimer la facture associée

## Mise en place
Après la récupération du projet sur GitLab, il faut s'assurer de la présence de Composer, Docker et Docker-Composer sur sa machine.
Ensuite, on commence par l'installation des dépendances du projet avec la commande suivante :
```sh
composer install
```

Il ne reste plus qu'à lancer le script de mise en place :
```sh
./setup.sh
```
Il construit l'image Docker qui est utilisé par le projet. Une fois l'image construite, il va lancer un docker-compose pour utiliser cette image.
Et lorsque  ce conteneur sera actif, il lancera le script de création de la base de données de développement et de test.

Le conteneur de cette image aura pour nom `symfony`. Si vous souhaitez modifier ce nom, il est défini dans le fichier `docker/docker-compose.yml`

## Testing
Les tests unitaires et fonctionnelles utilisent PHPUnit. Pour exécuter tous les tests existants, il suffit de lancer la commande suivante :
```sh
docker container exec -t -w /symfony symfony php bin/phpunit
```
Bien sûr, si vous avez modifier le nom du conteneur, remplacez `symfony` par le nom que vous avez choisi. 

## Nettoyage
Vouys désirez supprimer les élements installés lors de la mise en place du projet, rien de plus simple. Il faut exécuter : 
```sh
./cleanup.sh
```
Il supprimera les données Docker de ce projet ainsi que les bases de données mises en place.

## Troubleshooting
- Si les scripts de mises en place ou de nettoyages ne se lancent pas, vérifier qu'ils peuvent bien être exécuter
- Les problèmes d'écritures proviennent en grande majorité de droits d'écritures invalides