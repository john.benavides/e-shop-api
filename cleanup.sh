docker-compose -f docker/docker-compose.yml down --remove-orphans
docker image rm symfony/php
echo "Si vous n'utilisez pas l'image Docker <php:8.1-apache>, elle doit être également supprimée"
echo "avec la commande : docker image rm php:8.1-apache"

rm var/data.db
rm var/data_test.db