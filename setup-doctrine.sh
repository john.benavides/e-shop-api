#!/bin/bash
php bin/console doctrine:database:create
echo "yes" | php bin/console make:migration
echo "yes" | php bin/console doctrine:migrations:migrate

php bin/console --env=test doctrine:database:create
php bin/console --env=test doctrine:schema:create
echo "yes" | php bin/console --env=test doctrine:fixtures:load