<?php

namespace App\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;

class ProductService
{
    public function getUnavailableProducts(Basket $basket)
    {
        $unavailableProducts = $basket->getProducts()
            ->filter(fn(BasketProduct $bp) => !$bp->getProduct()->isAvailable())
            ->map(fn(BasketProduct $bp) => $bp->getProduct())
        ;

        return $unavailableProducts;
    }

    public function getOutOfStockProducts(Basket $basket)
    {
        $outOfStockProducts = $basket->getProducts()
            ->filter(fn(BasketProduct $bp) => $bp->getQuantity() > $bp->getProduct()->getStock())
            ->map(fn(BasketProduct $bp) => $bp->getProduct())
        ;

        return $outOfStockProducts;
    }
}