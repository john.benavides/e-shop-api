<?php

namespace App\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;

class StockService
{
    public function addToStock(Basket $basket)
    {
        /** @var BasketProduct */
        foreach($basket->getProducts() as $basketProduct)
        {
            $product = $basketProduct->getProduct();

            $product->setStock($product->getStock() + $basketProduct->getQuantity());
        }
    }

    public function removeFromStock(Basket $basket)
    {
        /** @var BasketProduct */
        foreach($basket->getProducts() as $basketProduct)
        {
            $product = $basketProduct->getProduct();

            $product->setStock($product->getStock() - $basketProduct->getQuantity());
        }
    }
}