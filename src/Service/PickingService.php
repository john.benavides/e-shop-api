<?php

namespace App\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use Exception;

class PickingService
{
    public function addProductToBasket(Basket $basket, Product $product, int $quantity): void
    {
        if (!$product->isAvailable())
            throw new Exception('The selected product is not available.');

        if ($quantity <= 0)
            throw new Exception('The chosen quantity is 0 or less than 0.');

        if ($quantity > $product->getStock())
            throw new Exception('The chosen quantity is greater than our current stock.');

        /** @var BasketProduct|false */
        $basketProduct = $basket->getProducts()
            ->filter(fn(BasketProduct $bp) => $bp->getProduct() === $product)
            ->first();

        if ($basketProduct)
        {
            $basketProduct->setQuantity($quantity);
            return;
        }

        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity($quantity);

        $basket->addProduct($basketProduct);
    }

    public function removeProductFromBasket(Basket $basket, Product $product): void
    {
        /** @var BasketProduct|false */
        $basketProduct = $basket->getProducts()
            ->filter(fn(BasketProduct $bp) => $bp->getProduct() === $product)
            ->first();

        if (!$basketProduct)
        {
            throw new Exception('The selected product is not in the basket.');
        }

        $basket->removeProduct($basketProduct);
    }
}