<?php

namespace App\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;

class ValueService
{
    public function getBasketValue(Basket $basket): float
    {
        $value = 0.0;

        /** @var BasketProduct */
        foreach($basket->getProducts() as $basketProduct)
        {
            $product = $basketProduct->getProduct();
            $quantity = $basketProduct->getQuantity();

            $value += ($product->getPrice() * $quantity);
        }

        return ceil($value * 100) / 100;
    }
}