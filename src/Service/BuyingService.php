<?php

namespace App\Service;

use App\Entity\Basket;
use App\Entity\Invoice;
use App\Entity\Product;
use App\Enum\BasketStatus;
use App\Enum\InvoiceStatus;
use App\Enum\PaymentMean;
use DateTimeImmutable;
use Exception;

class BuyingService
{
    public function __construct(
        private ProductService $productService,
        private ValueService $valueService,
        private StockService $stockService,
    ) { }

    public function createInvoice(Basket $basket): Invoice
    {
        if ($basket->getProducts()->isEmpty())
        {
            throw new Exception('The current basket is empty.');
        }

        $unavailableProducts = $this->productService->getUnavailableProducts($basket);

        if ($unavailableProducts->count() > 0)
        {
            $count = $unavailableProducts->count();
            $productsName = $unavailableProducts->map(fn(Product $p) => "'" . $p->getName() . "'")->toArray();

            $message = sprintf(
                '%s %s unavailable.',
                implode(', ', $productsName),
                $count > 1 ? 'are' : 'is'
            );
            throw new Exception($message);
        }

        $outOfStockProducts = $this->productService->getOutOfStockProducts($basket);

        if ($outOfStockProducts->count() > 0)
        {
            $count = $outOfStockProducts->count();
            $productsName = $outOfStockProducts->map(fn(Product $p) => "'" . $p->getName() . "'")->toArray();

            $message = sprintf('There is not enought %s to fulfill this basket.', implode(', ', $productsName));
            throw new Exception($message);
        }

        $this->stockService->removeFromStock($basket);
        $basket->setStatus(BasketStatus::VALIDATED);

        $invoice = new Invoice;
        $invoice->setCreatedAt(new DateTimeImmutable);
        $invoice->setBasket($basket);
        $invoice->setTotal($this->valueService->getBasketValue($basket));

        return $invoice;
    }

    public function payInvoice(Invoice $invoice, PaymentMean $mean)
    {
        $invoice->setPaymentMean($mean);
        $invoice->setStatus(InvoiceStatus::PAID);
    }

    public function cancelInvoice(Invoice $invoice): void
    {
        $invoice->setStatus(InvoiceStatus::CANCELLED);
        $this->stockService->addToStock($invoice->getBasket());        
    }
}