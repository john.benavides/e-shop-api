<?php

namespace App\Entity;

use App\Repository\BasketProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BasketProductRepository::class)
 */
class BasketProduct
{
    /**
     * @Ignore
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Ignore
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity=Basket::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $basket;

    /**
     * @Assert\Positive
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @Assert\NotNull
     * @ORM\ManyToOne(targetEntity=Product::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBasket(): ?Basket
    {
        return $this->basket;
    }

    public function setBasket(?Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
