<?php

namespace App\Entity;

use App\Enum\BasketStatus;
use App\Repository\BasketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * @ORM\Entity(repositoryClass=BasketRepository::class)
 */
class Basket
{
    /**
     * @Ignore
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, enumType=BasketStatus::class)
     */
    private $status = BasketStatus::PENDING;

    /**
     * @ORM\OneToMany(targetEntity=BasketProduct::class, mappedBy="basket", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?BasketStatus
    {
        return $this->status;
    }

    public function setStatus(BasketStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, BasketProduct>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(BasketProduct $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setBasket($this);
        }

        return $this;
    }

    public function removeProduct(BasketProduct $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getBasket() === $this) {
                $product->setBasket(null);
            }
        }

        return $this;
    }
}
