<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Throwable;

class ErrorController extends AbstractController
{
    public function show(Throwable $exception, ?DebugLoggerInterface $logger)
    {
        $statusCode = 500;

        if (is_a($exception, HttpException::class))
        {
            /**
             * @var int $statusCode
             * @var HttpException $exception
             */
            $statusCode = $exception->getStatusCode();
        }

        return new JsonResponse($exception->getMessage(), $statusCode);
    }
}