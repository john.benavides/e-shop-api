<?php

namespace App\Controller\Frontend;

use App\Entity\Basket;
use App\Entity\Invoice;
use App\Enum\BasketStatus;
use App\Enum\InvoiceStatus;
use App\Enum\PaymentMean;
use App\Service\BuyingService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/invoice", name="front_invoice_")
 */
class InvoiceController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private BuyingService $buyingService,
    ) { }

    /**
     * @Route("s", name="list", methods={"GET"})
     */
    public function list()
    {
        $invoices = $this->em->getRepository(Invoice::class)->findAll();

        return $this->json($invoices);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Invoice $invoice)
    {
        return $this->json($invoice);
    }

    /**
     * @Route("", name="create", methods={"POST"})
     */
    public function create(): Response
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        try
        {
            $invoice = $this->buyingService->createInvoice($basket);
        }
        catch (Exception $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }

        $this->em->persist($invoice);
        $this->em->flush();

        return $this->json($invoice, 201);
    }

    /**
     * @Route("/{id}", name="update", methods={"PATCH"})
     */
    public function update(Invoice $invoice, Request $request): Response
    {
        $json = $request->getContent();

        if (empty($json))
            throw new BadRequestHttpException('Body request is empty.');

        $object = json_decode($json);

        if (empty($object))
            throw new BadRequestHttpException('JSON syntax is invalid.');

        if (!property_exists($object, 'action') || !in_array($object->action, ['pay', 'cancel']))
        {
            throw new BadRequestHttpException('No action or the action is not recognized.');
        }

        if ($invoice->getStatus() !== InvoiceStatus::PENDING)
        {
            throw new BadRequestHttpException('This invoice is not pending, it cannot be further modified.');
        }

        if ('pay' === $object->action && (!property_exists($object, 'paymentMean') || !is_string($object->paymentMean)))
        {
            throw new BadRequestHttpException('No means of payment or the mean of payment is not a string.');
        }

        if ('pay' === $object->action && null === PaymentMean::tryFrom($object->paymentMean))
        {
            throw new BadRequestHttpException('The mean of payment is unknown.');
        }

        try
        {
            match($object->action)
            {
                'pay' => $this->buyingService->payInvoice($invoice, PaymentMean::from($object->paymentMean)),
                'cancel' => $this->buyingService->cancelInvoice($invoice),
            };
        }
        catch (Exception $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }

        $this->em->flush();
        
        return $this->json($invoice);
    }
}