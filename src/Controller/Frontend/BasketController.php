<?php

namespace App\Controller\Frontend;

use App\Entity\Basket;
use App\Entity\Product;
use App\Enum\BasketStatus;
use App\Service\PickingService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/basket", name="front_basket_")
 */
class BasketController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
    ) { }

    /**
     * @Route("", name="list_content", methods={"GET"})
     */
    public function listContent()
    {
        /** @var Basket|null */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]);

        if (null === $basket)
        {
            $basket = new Basket;
            $this->em->persist($basket);
            $this->em->flush();
        }

        return $this->json($basket->getProducts());
    }

    /**
     * @Route("", name="update", methods={"PATCH"})
     */
    public function update(Request $request, PickingService $pickingService)
    {
        $json = $request->getContent();

        if (empty($json))
            throw new BadRequestHttpException('Body request is empty.');

        $object = json_decode($json);

        if (empty($object))
            throw new BadRequestHttpException('JSON syntax is invalid.');

        if (!property_exists($object, 'action') || !in_array($object->action, ['add-product', 'remove-product']))
        {
            throw new BadRequestHttpException('No action or the action is not recognized.');
        }

        if ('add-product' === $object->action && (!property_exists($object, 'quantity') || !is_int($object->quantity)))
        {
            throw new BadRequestHttpException('No quantity or the quantity is not a number.');
        }

        if (!property_exists($object, 'product') || !is_int($object->product))
        {
            throw new BadRequestHttpException('No product or the product is not an ID.');
        }

        $product = $this->em->getRepository(Product::class)->find($object->product);

        if (null === $product)
        {
            throw new BadRequestHttpException('The chosen product does not exist.');
        }

        /** @var Basket|null */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]);

        if (null === $basket)
        {
            $basket = new Basket;
        }

        try
        {
            match($object->action)
            {
                'add-product' => $pickingService->addProductToBasket($basket, $product, $object->quantity),
                'remove-product' => $pickingService->removeProductFromBasket($basket, $product),
            };
        }
        catch (Exception $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }

        $this->em->persist($basket);
        $this->em->flush();
        
        return $this->json($basket->getProducts());
    }
}