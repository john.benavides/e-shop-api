<?php

namespace App\Controller\Frontend;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/product", name="front_product_")
 */
class ProductController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
    ) { }

    /**
     * @Route("s", name="list")
     */
    public function list()
    {
        $products = $this->em->getRepository(Product::class)->findAll();

        return $this->json($products);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Product $product)
    {
        return $this->json($product);
    }
}