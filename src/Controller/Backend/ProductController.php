<?php

namespace App\Controller\Backend;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/product", name="back_product_")
 */
class ProductController extends AbstractController
{
    // TODO: Factorize raw product check

    public function __construct(
        private EntityManagerInterface $em,
    ) { }

    /**
     * @Route("", name="create", methods={"POST"})
     */
    public function create(Request $request)
    {
        $json = $request->getContent();

        if (empty($json))
            throw new BadRequestHttpException('Body request is empty.');

        $object = json_decode($json);

        if (empty($object))
            throw new BadRequestHttpException('JSON syntax is invalid.');

        $errors = [];
        $expected = [
            'name' => ['string'],
            'price' => ['double', 'integer'],
            'available' => ['boolean'],
            'stock' => ['integer'],
        ];

        foreach($expected as $property => $types)
        {
            if (!property_exists($object, $property))
                $errors[] = $property;
            else if (!in_array(gettype($object->$property), $types))
                $errors[] = $property;
        }

        if (!empty($errors))
        {
            $message = sprintf(
                'Product properties are missing or using the wrong type. (properties: %s)',
                implode(', ', $errors)
            );
            throw new BadRequestHttpException($message);
        }

        $product = new Product;
        $product->setName($object->name);
        $product->setPrice($object->price);
        $product->setAvailable($object->available);
        $product->setStock($object->stock);

        $this->em->persist($product);
        $this->em->flush();

        return $this->json($product, 201);
    }

    /**
     * @Route("/{id}", name="update", methods={"PUT"})
     */
    public function update(Product $product, Request $request)
    {
        $json = $request->getContent();

        if (empty($json))
            throw new BadRequestHttpException('Body request is empty.');

        $object = json_decode($json);

        if (empty($object))
            throw new BadRequestHttpException('JSON syntax is invalid.');

        $errors = [];
        $expected = [
            'name' => ['string'],
            'price' => ['double', 'integer'],
            'available' => ['boolean'],
            'stock' => ['integer'],
        ];

        foreach($expected as $property => $types)
        {
            if (!property_exists($object, $property))
                $errors[] = $property;
            else if (!in_array(gettype($object->$property), $types))
                $errors[] = $property;
        }

        if (!empty($errors))
        {
            $message = sprintf(
                'Product properties are missing or using the wrong type. (properties: %s)',
                implode(', ', $errors)
            );
            throw new BadRequestHttpException($message);
        }

        $product->setName($object->name);
        $product->setPrice($object->price);
        $product->setAvailable($object->available);
        $product->setStock($object->stock);

        $this->em->flush();

        return $this->json($product);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Product $product)
    {
        $this->em->remove($product);
        $this->em->flush();

        return $this->json($product);
    }
}