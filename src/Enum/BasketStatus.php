<?php

namespace App\Enum;

enum BasketStatus: string
{
    case VALIDATED = 'validated';
    case PENDING = 'pending';
}