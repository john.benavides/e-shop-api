<?php

namespace App\Enum;

enum PaymentMean: string
{
    case PAYPAL = 'paypal';
    case CREDIT_CARD = 'credit_card';
    case IN_STORE_CREDIT = 'in_store_credit';
}