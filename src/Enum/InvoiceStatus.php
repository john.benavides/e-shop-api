<?php

namespace App\Enum;

enum InvoiceStatus: string
{
    case PAID = 'paid';
    case PENDING = 'pending';
    case CANCELLED = 'cancelled';
}