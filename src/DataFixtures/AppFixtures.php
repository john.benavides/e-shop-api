<?php

namespace App\DataFixtures;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Invoice;
use App\Entity\Product;
use App\Enum\BasketStatus;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $product = new Product;
        $product->setName('Sheet');
        $product->setPrice(129.99);
        $product->setAvailable(true);
        $product->setStock(100);

        $secondProduct = new Product;
        $secondProduct->setName('Tablecloth');
        $secondProduct->setPrice(59.99);
        $secondProduct->setAvailable(true);
        $secondProduct->setStock(50);

        $unavailableProduct = new Product;
        $unavailableProduct->setName('Cover');
        $unavailableProduct->setPrice(29.99);
        $unavailableProduct->setAvailable(false);
        $unavailableProduct->setStock(5);

        $basket = new Basket;
        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(1);
        $basket->addProduct($basketProduct);

        $invoice = new Invoice;
        $invoiceBasket = new Basket;
        $invoiceBasket->setStatus(BasketStatus::VALIDATED);
        $invoiceBasketProduct = new BasketProduct;
        $invoiceBasketProduct->setProduct($product);
        $invoiceBasketProduct->setQuantity(1);
        $invoiceBasket->addProduct($invoiceBasketProduct);

        $invoice->setCreatedAt(new DateTimeImmutable);
        $invoice->setBasket($invoiceBasket);
        $invoice->setTotal($product->getPrice());

        $manager->persist($product);
        $manager->persist($secondProduct);
        $manager->persist($unavailableProduct);
        $manager->persist($basket);
        $manager->persist($invoice);

        $manager->flush();
    }
}
