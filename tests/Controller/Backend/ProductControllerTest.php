<?php

namespace App\Tests\Controller\Backend;

use DAMA\DoctrineTestBundle\Doctrine\DBAL\StaticDriver;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    /** @var KernelBrowser */
    private $client;

    public static function setUpBeforeClass(): void
    {
        StaticDriver::setKeepStaticConnections(false);
    }

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public static function tearDownAfterClass(): void
    {
        StaticDriver::setKeepStaticConnections(true);
    }

    public function testCreateValid(): int
    {
        $rawProduct = [
            'name' => 'Toy',
            'price' => 5.5,
            'available' => true,
            'stock' => 11,
        ];

        $this->client->request('POST', '/api/product', [], [], [], json_encode($rawProduct));
        $response = $this->client->getResponse();

        $this->assertEquals(201, $response->getStatusCode());
        $product = json_decode($response->getContent());
        $this->assertIsObject($product);
        $this->assertObjectHasAttribute('id', $product);
        $this->assertObjectHasAttribute('name', $product);
        $this->assertObjectHasAttribute('price', $product);
        $this->assertObjectHasAttribute('available', $product);
        $this->assertObjectHasAttribute('stock', $product);
        $this->assertIsInt($product->id);
        $this->assertIsString($product->name);
        $this->assertIsFloat($product->price);
        $this->assertIsBool($product->available);
        $this->assertIsInt($product->stock);
        $this->assertEquals($rawProduct['name'], $product->name);
        $this->assertEquals($rawProduct['price'], $product->price);
        $this->assertEquals($rawProduct['available'], $product->available);
        $this->assertEquals($rawProduct['stock'], $product->stock);


        return $product->id;
    }

    /**
     * @depends testCreateValid
     */
    public function testUpdateValid(int $id): int
    {
        $rawProduct = [
            'name' => 'Toy 2',
            'price' => 12.5,
            'available' => false,
            'stock' => 22,
        ];

        $this->client->request('PUT', '/api/product/' . $id, [], [], [], json_encode($rawProduct));
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $product = json_decode($response->getContent());
        $this->assertIsObject($product);
        $this->assertObjectHasAttribute('id', $product);
        $this->assertObjectHasAttribute('name', $product);
        $this->assertObjectHasAttribute('price', $product);
        $this->assertObjectHasAttribute('available', $product);
        $this->assertObjectHasAttribute('stock', $product);
        $this->assertIsInt($product->id);
        $this->assertIsString($product->name);
        $this->assertIsFloat($product->price);
        $this->assertIsBool($product->available);
        $this->assertIsInt($product->stock);
        $this->assertEquals($id, $product->id);
        $this->assertEquals($rawProduct['name'], $product->name);
        $this->assertEquals($rawProduct['price'], $product->price);
        $this->assertEquals($rawProduct['available'], $product->available);
        $this->assertEquals($rawProduct['stock'], $product->stock);

        return $product->id;
    }

    /**
     * @depends testUpdateValid
     */
    public function testDeleteValid(int $id): void
    {
        $this->client->request('DELETE', '/api/product/' . $id);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $this->client->request('GET', '/api/product/' . $id);
        $responseNotFound = $this->client->getResponse();

        $this->assertEquals(404, $responseNotFound->getStatusCode());
    }
}