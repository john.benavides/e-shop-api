<?php

namespace App\Tests\Controller\Frontend;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use App\Enum\BasketStatus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BasketControllerTest extends WebTestCase
{
    private $client;

    /** @var EntityManagerInterface */
    private $em;

    private $products;
    private $productsID;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->em = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        $this->products = $this->em->getRepository(Product::class)->findAll();
        $this->productsID = array_map(fn($p) => $p->getId(), $this->products);
    }

    public function testListContentValid(): void
    {
        $this->client->request('GET', '/api/basket');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $basketProducts = json_decode($response->getContent());

        $this->assertIsArray($basketProducts);
        
        foreach($basketProducts as $basketProduct)
        {
            $this->assertIsObject($basketProduct);
            $this->assertObjectHasAttribute('quantity', $basketProduct);
            $this->assertObjectHasAttribute('product', $basketProduct);
            $this->assertIsInt($basketProduct->quantity);
            $this->assertGreaterThan(0, $basketProduct->quantity);
            $this->assertIsObject($basketProduct->product);
            $product = $basketProduct->product;

            $this->assertObjectHasAttribute('id', $product);
            $this->assertIsInt($product->id);
            $this->assertObjectHasAttribute('name', $product);
            $this->assertObjectHasAttribute('price', $product);
            $this->assertObjectHasAttribute('available', $product);
            $this->assertObjectHasAttribute('stock', $product);
            $this->assertIsString($product->name);
            $this->assertIsFloat($product->price);
            $this->assertGreaterThan(0, $basketProduct->quantity);
            $this->assertIsBool($product->available);
            $this->assertIsInt($product->stock);
            $this->assertGreaterThanOrEqual(0, $basketProduct->quantity);
        }
    }

    public function testChangeProductFromStockValid(): void
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        /** @var BasketProduct */
        $changeableBasketProduct = $basket->getProducts()
            ->filter(fn(BasketProduct $bp) => $bp->getProduct()->isAvailable())
            ->first()
        ;

        $product = $changeableBasketProduct->getProduct();

        do {
            $newQuantity = rand(0, $product->getStock());
        } while($changeableBasketProduct->getQuantity() === $newQuantity);

        $content = [
            'action' => 'add-product',
            'product' => $product->getId(),
            'quantity' => $newQuantity,
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $basketProducts = json_decode($response->getContent());

        $modifiedBasketProducts = array_filter($basketProducts, fn($bp) => $product->getId() === $bp->product->id);
        $modifiedBasketProduct = reset($modifiedBasketProducts);

        $this->assertEquals($newQuantity, $modifiedBasketProduct->quantity);
    }

    public function testChangeProductFromStockNonExistentProduct()
    {
        $content = [
            'action' => 'add-product',
            'product' => -1,
            'quantity' => 1,
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The chosen product does not exist.', $message);
    }

    public function testRemoveProductFromStockValid()
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        /** @var BasketProduct */
        $basketProduct = $basket->getProducts()->first();
        $product = $basketProduct->getProduct();

        $content = [
            'action' => 'remove-product',
            'product' => $product->getId(),
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $basketProducts = json_decode($response->getContent());

        $removedProducts = array_filter($basketProducts, fn($bp) => $product->getId() === $bp->product->id);
        $this->assertCount(0, $removedProducts);
    }

    public function testRemoveProductFromStockNotInStock(): void
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        $productsInBasketID = $basket->getProducts()
            ->map(fn(BasketProduct $bp) => $bp->getProduct()->getId())
            ->toArray()
        ;

        $usableProductsID = array_diff($this->productsID, $productsInBasketID);

        $content = [
            'action' => 'remove-product',
            'product' => reset($usableProductsID),
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The selected product is not in the basket.', $message);
    }

    public function testAddProductToStockValid(): void
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        $productsInBasketID = $basket->getProducts()
            ->map(fn(BasketProduct $bp) => $bp->getProduct()->getId())
            ->toArray()
        ;

        $usableProductsID = array_diff($this->productsID, $productsInBasketID);
        $usableProducts = array_filter($this->products, fn(Product $p) => in_array($p->getId(), $usableProductsID));
        
        $usableProduct = reset($usableProducts);

        $content = [
            'action' => 'add-product',
            'product' => $usableProduct->getId(),
            'quantity' => $usableProduct->getStock(),
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $basketProducts = json_decode($response->getContent());

        $addedBasketProducts = array_filter($basketProducts, fn($bp) => $usableProduct->getId() === $bp->product->id);
        $addedBasketProduct = reset($addedBasketProducts);
        $this->assertCount(1, $addedBasketProducts);
        $this->assertEquals($usableProduct->getStock(), $addedBasketProduct->quantity);
    }

    public function testAddProductToStockOutOfStock(): void
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        $productsInBasketID = $basket->getProducts()
            ->map(fn(BasketProduct $bp) => $bp->getProduct()->getId())
            ->toArray()
        ;

        $usableProductsID = array_diff($this->productsID, $productsInBasketID);
        $usableProducts = array_filter(
            $this->products,
            fn(Product $p) => $p->isAvailable() && in_array($p->getId(), $usableProductsID)
        );
        $usableProduct = reset($usableProducts);

        $content = [
            'action' => 'add-product',
            'product' => $usableProduct->getId(),
            'quantity' => $usableProduct->getStock() + 1,
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The chosen quantity is greater than our current stock.', $message);
    }

    public function testAddProductToStockNotAvailable(): void
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        $unavailableProducts = array_filter($this->products, fn(Product $p) => !$p->isAvailable());
        $unavailableProduct = reset($unavailableProducts);

        $content = [
            'action' => 'add-product',
            'product' => $unavailableProduct->getId(),
            'quantity' => 1,
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The selected product is not available.', $message);
    }

    public function testAddProductToStockZeroOrLess(): void
    {
        /** @var Basket */
        $basket = $this->em->getRepository(Basket::class)->findOneBy([
            'status' => BasketStatus::PENDING,
        ]) ?? new Basket;

        $availableProducts = array_filter($this->products, fn(Product $p) => $p->isAvailable());
        $availableProduct = reset($availableProducts);

        $content = [
            'action' => 'add-product',
            'product' => $availableProduct->getId(),
            'quantity' => 0,
        ];

        $this->client->request('PATCH', '/api/basket', [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The chosen quantity is 0 or less than 0.', $message);
    }
}