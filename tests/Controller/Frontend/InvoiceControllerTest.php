<?php

namespace App\Tests\Controller\Frontend;

use App\Entity\Basket;
use App\Entity\Invoice;
use App\Enum\BasketStatus;
use App\Enum\InvoiceStatus;
use App\Enum\PaymentMean;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InvoiceControllerTest extends WebTestCase
{
    private $client;

    /** @var EntityManagerInterface */
    private $em;

    private $invoiceRepository;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->em = $this->client->getContainer()->get('doctrine.orm.entity_manager');
        $this->basketRepository = $this->em->getRepository(Basket::class);
        $this->invoiceRepository = $this->em->getRepository(Invoice::class);
    }

    public function testCreateValid(): void
    {
        $this->client->request('POST', '/api/invoice');
        $response = $this->client->getResponse();

        $this->assertEquals(201, $response->getStatusCode());
        $invoice = json_decode($response->getContent());

        $this->assertIsObject($invoice);
        $this->assertObjectHasAttribute('status', $invoice);
        $this->assertIsString($invoice->status);
        $this->assertEquals(InvoiceStatus::PENDING, InvoiceStatus::from($invoice->status));
        $this->assertObjectHasAttribute('paymentMean', $invoice);
        $this->assertEquals(null, $invoice->paymentMean);
        $this->assertObjectHasAttribute('total', $invoice);
        $this->assertIsFloat($invoice->total);
        $this->assertEquals(129.99, $invoice->total);
    }

    public function testCreateEmptyBasket(): void
    {
        $basket = $this->basketRepository->findOneBy(['status' => BasketStatus::PENDING]);
        $basketProduct = $basket->getProducts()->first();
        $basket->removeProduct($basketProduct);
        $this->em->flush();

        $this->client->request('POST', '/api/invoice');
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The current basket is empty.', $message);
    }

    public function testCreateUnavailable(): void
    {
        $basket = $this->basketRepository->findOneBy(['status' => BasketStatus::PENDING]);
        $product = $basket->getProducts()->first()->getProduct();
        $product->setAvailable(false);
        $this->em->flush();

        $this->client->request('POST', '/api/invoice');
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals("'Sheet' is unavailable.", $message);
    }

    public function testCreateOutOfStock(): void
    {
        $basket = $this->basketRepository->findOneBy(['status' => BasketStatus::PENDING]);
        $basketProduct = $basket->getProducts()->first();
        $basketProduct->setQuantity($basketProduct->getProduct()->getStock() + 1);
        $this->em->flush();

        $this->client->request('POST', '/api/invoice');
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals("There is not enought 'Sheet' to fulfill this basket.", $message);
    }

    public function testUpdateNotPending(): void
    {
        $invoice = $this->invoiceRepository->findOneBy([]);
        $invoice->setStatus(InvoiceStatus::CANCELLED);
        $this->em->flush();

        $content = [
            'action' => 'cancel',
        ];

        $this->client->request('PATCH', '/api/invoice/' . $invoice->getId(), [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('This invoice is not pending, it cannot be further modified.', $message);
    }

    public function testUpdatePayValid(): void
    {
        $invoice = $this->invoiceRepository->findOneBy(['status' => InvoiceStatus::PENDING]);

        $content = [
            'action' => 'pay',
            'paymentMean' => PaymentMean::PAYPAL->value,
        ];

        $this->client->request('PATCH', '/api/invoice/' . $invoice->getId(), [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $invoice = json_decode($response->getContent());

        $this->assertIsObject($invoice);
        $this->assertObjectHasAttribute('status', $invoice);
        $this->assertIsString($invoice->status);
        $this->assertEquals(InvoiceStatus::PAID, InvoiceStatus::from($invoice->status));
        $this->assertObjectHasAttribute('paymentMean', $invoice);
        $this->assertIsString($invoice->paymentMean);
        $this->assertEquals(PaymentMean::PAYPAL, PaymentMean::from($invoice->paymentMean));
    }

    public function testUpdatePayNoPaymentMean(): void
    {
        $invoice = $this->invoiceRepository->findOneBy(['status' => InvoiceStatus::PENDING]);
        $content = [
            'action' => 'pay',
        ];

        $this->client->request('PATCH', '/api/invoice/' . $invoice->getId(), [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('No means of payment or the mean of payment is not a string.', $message);
    }

    public function testUpdatePayUnknownPaymentMean(): void
    {
        $invoice = $this->invoiceRepository->findOneBy(['status' => InvoiceStatus::PENDING]);

        $content = [
            'action' => 'pay',
            'paymentMean' => 'wechat',
        ];

        $this->client->request('PATCH', '/api/invoice/' . $invoice->getId(), [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $message = json_decode($response->getContent());
        $this->assertEquals('The mean of payment is unknown.', $message);

    }

    public function testUpdateCancelValid(): void
    {
        $invoice = $this->invoiceRepository->findOneBy(['status' => InvoiceStatus::PENDING]);

        $content = [
            'action' => 'cancel',
        ];

        $this->client->request('PATCH', '/api/invoice/' . $invoice->getId(), [], [], [], json_encode($content));
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $invoice = json_decode($response->getContent());

        $this->assertIsObject($invoice);
        $this->assertObjectHasAttribute('status', $invoice);
        $this->assertIsString($invoice->status);
        $this->assertEquals(InvoiceStatus::CANCELLED, InvoiceStatus::from($invoice->status));
    }


}