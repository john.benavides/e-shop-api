<?php

namespace App\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    private $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testListValid(): int
    {
        $this->client->request('GET', '/api/products');
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $products = json_decode($response->getContent());
        $this->assertIsArray($products);

        foreach($products as $product)
        {
            $this->assertIsObject($product);
            $this->assertObjectHasAttribute('id', $product);
            $this->assertObjectHasAttribute('name', $product);
            $this->assertObjectHasAttribute('price', $product);
            $this->assertObjectHasAttribute('available', $product);
            $this->assertObjectHasAttribute('stock', $product);
            $this->assertIsInt($product->id);
            $this->assertIsString($product->name);
            $this->assertIsFloat($product->price);
            $this->assertIsBool($product->available);
            $this->assertIsInt($product->stock);
        }

        return $product->id;
    }

    /**
     * @depends testListValid
     */
    public function testShowValid(int $id): void
    {
        $this->client->request('GET', '/api/product/' . $id);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $product = json_decode($response->getContent());
        $this->assertIsObject($product);
        $this->assertObjectHasAttribute('id', $product);
        $this->assertObjectHasAttribute('name', $product);
        $this->assertObjectHasAttribute('price', $product);
        $this->assertObjectHasAttribute('available', $product);
        $this->assertObjectHasAttribute('stock', $product);
        $this->assertEquals($id, $product->id);
        $this->assertIsString($product->name);
        $this->assertIsFloat($product->price);
        $this->assertIsBool($product->available);
        $this->assertIsInt($product->stock);
    }
}