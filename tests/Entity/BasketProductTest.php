<?php

namespace App\Tests\Entity;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;
use TypeError;

class BasketProductTest extends TestCase
{
    private $validator;
    private $basketProduct;

    public function setUp(): void
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $this->basketProduct = new BasketProduct;
    }

    public function testId(): void
    {
        $this->assertEquals(null, $this->basketProduct->getId());
    }

    public function testBasketValid(): void
    {
        $basket = new Basket;

        $this->basketProduct->setBasket($basket);
        $this->assertEquals($basket, $this->basketProduct->getBasket());
    }

    public function testBasketNull(): void
    {
        $this->basketProduct->setBasket(null);
        $constraintViolationList = $this->validator->validateProperty($this->basketProduct, 'basket');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testQuantityValid(): void
    {
        $this->basketProduct->setQuantity(3);
        $this->assertEquals(3, $this->basketProduct->getQuantity());
    }

    public function testQuantityNull(): void
    {
        $this->expectException(TypeError::class);
        $this->basketProduct->setQuantity(null);
    }

    public function testQuantityZero(): void
    {
        $this->basketProduct->setQuantity(0);
        $constraintViolationList = $this->validator->validateProperty($this->basketProduct, 'quantity');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testQuantityNegative(): void
    {
        $this->basketProduct->setQuantity(-39);
        $constraintViolationList = $this->validator->validateProperty($this->basketProduct, 'quantity');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testProductValid(): void
    {
        $product = new Product;

        $this->basketProduct->setProduct($product);
        $this->assertEquals($product, $this->basketProduct->getProduct());
    }

    public function testProductNull(): void
    {
        $this->basketProduct->setProduct(null);
        $constraintViolationList = $this->validator->validateProperty($this->basketProduct, 'product');
        $this->assertEquals(1, $constraintViolationList->count());
    }
}
