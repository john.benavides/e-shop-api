<?php

namespace App\Tests\Entity;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Invoice;
use App\Entity\Product;
use App\Enum\BasketStatus;
use App\Enum\InvoiceStatus;
use App\Enum\PaymentMean;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use TypeError;

class InvoiceTest extends TestCase
{
    private $validator;
    private $invoice;

    public function setUp(): void
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $this->invoice = new Invoice;
    }

    public function testId(): void
    {
        $this->assertEquals(null, $this->invoice->getId());
    }

    public function testCreatedAtValid(): void
    {
        $date = new DateTimeImmutable;

        $this->invoice->setCreatedAt($date);
        $this->assertEquals($date, $this->invoice->getCreatedAt());
    }

    public function testCreatedAtNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setCreatedAt(null);
    }

    public function testUpdatedAtValid(): void
    {
        $date = new DateTimeImmutable;

        $this->invoice->setUpdatedAt($date);
        $this->assertEquals($date, $this->invoice->getUpdatedAt());
    }

    public function testUpdatedAtNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setUpdatedAt(null);
    }

    public function testBasketValid(): void
    {
        $basket = new Basket;

        $this->invoice->setBasket($basket);
        $this->assertEquals($basket, $this->invoice->getBasket());
    }

    public function testBasketNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setBasket(null);
    }

    public function testTotalValid(): void
    {
        $this->invoice->setTotal(3.5);
        $this->assertEquals(3.5, $this->invoice->getTotal());
    }

    public function testTotalNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setTotal(null);
    }

    public function testTotalZero(): void
    {
        $this->invoice->setTotal(0);
        $constraintViolationList = $this->validator->validateProperty($this->invoice, 'total');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testTotalNegative(): void
    {
        $this->invoice->setTotal(-39.2);
        $constraintViolationList = $this->validator->validateProperty($this->invoice, 'total');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testStatusValid(): void
    {
        $this->invoice->setStatus(InvoiceStatus::PENDING);
        $this->assertEquals(InvoiceStatus::PENDING, $this->invoice->getStatus());
    }

    public function testStatusNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setStatus(null);
    }

    public function testStatusString(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setStatus('pending');
    }

    public function testPaymentMeanValid(): void
    {
        $this->invoice->setPaymentMean(PaymentMean::PAYPAL);
        $this->assertEquals(PaymentMean::PAYPAL, $this->invoice->getPaymentMean());

        $this->invoice->setPaymentMean(null);
        $this->assertEquals(null, $this->invoice->getPaymentMean());
    }

    public function testPaymentMeanString(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setPaymentMean('paypal');
    }

    /*
    public function testStatusValid(): void
    {
        $this->invoice->setStatus(BasketStatus::PENDING);
        $this->assertEquals(BasketStatus::PENDING, $this->invoice->getStatus());
    }

    public function testStatusNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setStatus(null);
    }

    public function testStatusString(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->setStatus('pending');
    }

    public function testProductsValid(): void
    {
        $basketProduct = new BasketProduct;

        $this->invoice->addProduct($basketProduct);
        $this->assertEquals(1, $this->invoice->getProducts()->count());
        $this->assertEquals($basketProduct, $this->invoice->getProducts()->first());
    }

    public function testProductsNull(): void
    {
        $this->expectException(TypeError::class);
        $this->invoice->addProduct(null);
    }
    */
}
