<?php

namespace App\Tests\Entity;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Enum\BasketStatus;
use PHPUnit\Framework\TestCase;
use TypeError;

class BasketTest extends TestCase
{
    private $basket;

    public function setUp(): void
    {
        $this->basket = new Basket;
    }

    public function testId(): void
    {
        $this->assertEquals(null, $this->basket->getId());
    }

    public function testStatusValid(): void
    {
        $this->basket->setStatus(BasketStatus::PENDING);
        $this->assertEquals(BasketStatus::PENDING, $this->basket->getStatus());
    }

    public function testStatusNull(): void
    {
        $this->expectException(TypeError::class);
        $this->basket->setStatus(null);
    }

    public function testStatusString(): void
    {
        $this->expectException(TypeError::class);
        $this->basket->setStatus('pending');
    }

    public function testProductsValid(): void
    {
        $basketProduct = new BasketProduct;

        $this->basket->addProduct($basketProduct);
        $this->assertEquals(1, $this->basket->getProducts()->count());
        $this->assertEquals($basketProduct, $this->basket->getProducts()->first());
    }

    public function testProductsNull(): void
    {
        $this->expectException(TypeError::class);
        $this->basket->addProduct(null);
    }
}
