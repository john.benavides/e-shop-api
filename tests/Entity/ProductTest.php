<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;
use TypeError;

class ProductTest extends TestCase
{
    private $validator;
    private $product;

    public function setUp(): void
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping(true)
            ->addDefaultDoctrineAnnotationReader()
            ->getValidator();

        $this->product = new Product;
    }

    public function testId(): void
    {
        $this->assertEquals(null, $this->product->getId());
    }

    public function testNameValid(): void
    {
        $this->product->setName('Sheet');
        $this->assertEquals('Sheet', $this->product->getName());
    }

    public function testNameNull(): void
    {
        $this->expectException(TypeError::class);
        $this->product->setName(null);
    }

    public function testPriceValid(): void
    {
        $this->product->setPrice(129.99);
        $this->assertEquals(129.99, $this->product->getPrice());
    }

    public function testPriceNull(): void
    {
        $this->expectException(TypeError::class);
        $this->product->setPrice(null);
    }

    public function testPriceZero(): void
    {
        $this->product->setPrice(0);
        $constraintViolationList = $this->validator->validateProperty($this->product, 'price');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testPriceNegative(): void
    {
        $this->product->setPrice(-25.67);
        $constraintViolationList = $this->validator->validateProperty($this->product, 'price');
        $this->assertEquals(1, $constraintViolationList->count());
    }

    public function testAvailableValid(): void
    {
        $this->product->setAvailable(true);
        $this->assertEquals(true, $this->product->isAvailable());
    }

    public function testAvailableNull(): void
    {
        $this->expectException(TypeError::class);
        $this->product->setAvailable(null);
    }

    public function testStockValid(): void
    {
        $this->product->setStock(7);
        $this->assertEquals(7, $this->product->getStock());
    }

    public function testStockNull(): void
    {
        $this->expectException(TypeError::class);
        $this->product->setStock(null);
    }

    public function testStockZero(): void
    {
        $this->product->setStock(0);
        $constraintViolationList = $this->validator->validateProperty($this->product, 'stock');
        $this->assertEquals(0, $constraintViolationList->count());
    }

    public function testStockNegative(): void
    {
        $this->product->setStock(-42);
        $constraintViolationList = $this->validator->validateProperty($this->product, 'stock');
        $this->assertEquals(1, $constraintViolationList->count());
    }
}
