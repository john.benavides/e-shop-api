<?php

namespace App\Tests\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use App\Service\ProductService;
use App\Service\StockService;
use PHPUnit\Framework\TestCase;

class StockServiceTest extends TestCase
{
    public function testAddToStock(): void
    {
        $stockService = new StockService;
        
        $product = new Product;
        $product->setStock(25);
        
        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(20);
        
        $basket = new Basket;
        $basket->addProduct($basketProduct);

        $stockService->addToStock($basket);
        $this->assertEquals(45, $product->getStock());
    }

    public function testRemoveFromStock(): void
    {
        $stockService = new StockService;
        
        $product = new Product;
        $product->setStock(25);
        
        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(20);
        
        $basket = new Basket;
        $basket->addProduct($basketProduct);

        $stockService->removeFromStock($basket);
        $this->assertEquals(5, $product->getStock());
    }
}