<?php

namespace App\Tests\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use App\Service\ValueService;
use PHPUnit\Framework\TestCase;

class ValueServiceTest extends TestCase
{
    /**
     * @dataProvider basketValueProvider
     */
    public function testBasketValue($expected, $data)
    {
        $valueService = new ValueService;
        $basket = new Basket;

        foreach($data as $basketProductData)
        {
            $product = new Product;
            $product->setPrice($basketProductData[0]);

            $basketProduct = new BasketProduct;
            $basketProduct->setProduct($product);
            $basketProduct->setQuantity($basketProductData[1]);

            $basket->addProduct($basketProduct);
        }

        $this->assertEquals($expected, $valueService->getBasketValue($basket));
    }

    public function basketValueProvider()
    {
        return [
            'one product' => [30.0, [
                [6.0, 5],
            ]],
            'two product' => [100.0, [
                [2.0, 25],
                [5.0, 10],
            ]],
            'three product' => [58.91, [
                [1.99, 2],
                [4.99, 3],
                [9.99, 4],
            ]],
        ];
    }
}