<?php

namespace App\Tests\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use App\Service\ProductService;
use PHPUnit\Framework\TestCase;

class ProductServiceTest extends TestCase
{
    /**
     * @dataProvider unavailableProductsProvider
     */
    public function testUnavailableProducts($expected, $data)
    {
        $productService = new ProductService;
        $basket = new Basket;

        foreach($data as $available)
        {
            $product = new Product;
            $product->setAvailable($available);

            $basketProduct = new BasketProduct;
            $basketProduct->setProduct($product);

            $basket->addProduct($basketProduct);
        }

        $this->assertEquals($expected, $productService->getUnavailableProducts($basket)->count());
    }

    /**
     * @dataProvider outofStockProductsProvider
     */
    public function testOutofStockProducts($expected, $data)
    {
        $productService = new ProductService;
        $basket = new Basket;

        foreach($data as $basketProductData)
        {
            $product = new Product;
            $product->setStock($basketProductData[0]);

            $basketProduct = new BasketProduct;
            $basketProduct->setProduct($product);
            $basketProduct->setQuantity($basketProductData[1]);

            $basket->addProduct($basketProduct);
        }

        $this->assertEquals($expected, $productService->getOutofStockProducts($basket)->count());
    }

    public function unavailableProductsProvider()
    {
        return [
            'one unavailable' => [1, [false, true, true, true]],
            'two unavailable' => [2, [true, false, true, false]],
            'none unavailable' => [0, [true, true, true, true]],
        ];
    }

    public function outofStockProductsProvider()
    {
        return [
            'one out of stock' => [1, [
                [12, 23],
                [36, 9],
                [42, 1],
            ]],
            'two out of stock' => [2, [
                [1, 450],
                [4, 343],
                [5, 5],
            ]],
            'none out of stock' => [0, [
                [149, 23],
                [56, 4],
                [1023, 2],
            ]],
        ];
    }
}