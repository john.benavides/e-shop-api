<?php

namespace App\Tests\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Invoice;
use App\Entity\Product;
use App\Enum\InvoiceStatus;
use App\Service\BuyingService;
use App\Service\ProductService;
use App\Service\StockService;
use App\Service\ValueService;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class BuyingServiceTest extends TestCase
{
    public function testCancelInvoiceValid(): void
    {
        $buyingService = new BuyingService(new ProductService, new ValueService, new StockService);
        
        $product = new Product;
        $product->setAvailable(true);
        $product->setPrice(2.99);
        $product->setStock(15);        
        
        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(5);
        
        $basket = new Basket;
        $basket->addProduct($basketProduct);

        $invoice = new Invoice;
        $invoice->setBasket($basket);

        $buyingService->cancelInvoice($invoice);

        $this->assertLessThan(new DateTimeImmutable, $invoice->getUpdatedAt());
        $this->assertEquals(InvoiceStatus::CANCELLED, $invoice->getStatus());
        $this->assertEquals(20, $product->getStock());
    }

    public function testCreateInvoiceValid(): void
    {
        $buyingService = new BuyingService(new ProductService, new ValueService, new StockService);
        
        $product = new Product;
        $product->setAvailable(true);
        $product->setPrice(2.99);
        $product->setStock(15);        
        
        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(5);
        
        $basket = new Basket;
        $basket->addProduct($basketProduct);

        $invoice = $buyingService->createInvoice($basket);

        $this->assertInstanceOf(Invoice::class, $invoice);
        $this->assertLessThan(new DateTimeImmutable, $invoice->getCreatedAt());
        $this->assertEquals($basket, $invoice->getBasket());
        $this->assertEquals(14.95, $invoice->getTotal());
        $this->assertEquals(InvoiceStatus::PENDING, $invoice->getStatus());
        $this->assertEquals(10, $product->getStock());
    }

    /**
     * @dataProvider createInvoiceUnavailableProvider
     */
    public function testCreateInvoiceUnavailable($expected, $data): void
    {
        $buyingService = new BuyingService(new ProductService, new ValueService, new StockService);
        $basket = new Basket;
        
        foreach($data as $productData)
        {
            $product = new Product;
            $product->setName($productData[0]);
            $product->setAvailable($productData[1]);
            $product->setPrice(2.99);
            $product->setStock(10);        
            
            $basketProduct = new BasketProduct;
            $basketProduct->setProduct($product);
            $basketProduct->setQuantity(5);

            $basket->addProduct($basketProduct);
        }
        
        $this->expectExceptionMessage($expected);
        $buyingService->createInvoice($basket);
    }

    /**
     * @dataProvider createInvoiceOutOfStockProvider
     */
    public function testCreateInvoiceOutOfStock($expected, $data): void
    {
        $buyingService = new BuyingService(new ProductService, new ValueService, new StockService);
        $basket = new Basket;
        
        foreach($data as $productData)
        {
            $product = new Product;
            $product->setName($productData[0]);
            $product->setAvailable(true);
            $product->setPrice(2.99);
            $product->setStock($productData[1]);        
            
            $basketProduct = new BasketProduct;
            $basketProduct->setProduct($product);
            $basketProduct->setQuantity($productData[2]);

            $basket->addProduct($basketProduct);
        }
        
        $this->expectExceptionMessage($expected);
        $buyingService->createInvoice($basket);
    }

    public function createInvoiceUnavailableProvider()
    {
        return [
            'one unavailable' => ["'Sheet' is unavailable.", [
                ['Sheet', false],
                ['Cover', true],
            ]],
            'two unavailable' => ["'Sheet', 'Cover' are unavailable.", [
                ['Sheet', false],
                ['Cover', false],
            ]],
        ];
    }

    public function createInvoiceOutOfStockProvider()
    {
        return [
            'one out of stock' => ["There is not enought 'Sheet' to fulfill this basket.", [
                ['Sheet', 3, 5],
                ['Cover', 23, 2],
            ]],
            'two out of stock' => ["There is not enought 'Sheet', 'Cover' to fulfill this basket.", [
                ['Sheet', 4, 40],
                ['Cover', 2, 3],
            ]],
        ];
    }
}