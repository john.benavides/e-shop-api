<?php

namespace App\Tests\Service;

use App\Entity\Basket;
use App\Entity\BasketProduct;
use App\Entity\Product;
use App\Service\PickingService;
use PHPUnit\Framework\TestCase;

class PickingServiceTest extends TestCase
{
    public function testAddNewProductToBasketValid(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(true);
        $product->setStock(10);        

        $pickingService->addProductToBasket($basket, $product, 2);

        $basketProduct = $basket->getProducts()->first();
        $this->assertInstanceOf(BasketProduct::class, $basketProduct);
        $this->assertEquals($product, $basketProduct->getProduct());
        $this->assertEquals(2, $basketProduct->getQuantity());
    }

    public function testAddExistingProductToBasketValid(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(true);
        $product->setStock(10);

        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(1);

        $basket->addProduct($basketProduct);    

        $pickingService->addProductToBasket($basket, $product, 5);

        $this->assertEquals(1, $basket->getProducts()->count());
        $basketProduct = $basket->getProducts()->first();
        $this->assertEquals($product, $basketProduct->getProduct());
        $this->assertEquals(5, $basketProduct->getQuantity());
    }

    public function testAddProductToBasketProductNotAvailable(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(false);
        $product->setStock(10);        

        $this->expectExceptionMessage('The selected product is not available.');
        $pickingService->addProductToBasket($basket, $product, 2);
    }

    public function testAddProductToBasketQuantityInvalid(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(true);
        $product->setStock(10);        

        $this->expectExceptionMessage('The chosen quantity is 0 or less than 0.');
        $pickingService->addProductToBasket($basket, $product, -2);
    }

    public function testAddProductToBasketQuantityGreaterThanStock(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(true);
        $product->setStock(10);        

        $this->expectExceptionMessage('The chosen quantity is greater than our current stock.');
        $pickingService->addProductToBasket($basket, $product, 20);
    }

    public function testRemoveProductFromStockValid(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(true);
        $product->setStock(16);

        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(5);
        
        $basket->addProduct($basketProduct);

        $pickingService->removeProductFromBasket($basket, $product);

        $basketProduct = $basket->getProducts()->first();
        $this->assertEquals(false, $basketProduct);
        $this->assertCount(0, $basket->getProducts()->toArray());
    }

    public function testRemoveProductFromStockNotInStock(): void
    {
        $pickingService = new PickingService;
        $basket = new Basket;

        $product = new Product;
        $product->setAvailable(true);
        $product->setStock(16);

        $otherProduct = new Product;
        $otherProduct->setAvailable(false);
        $otherProduct->setStock(17);

        $basketProduct = new BasketProduct;
        $basketProduct->setProduct($product);
        $basketProduct->setQuantity(5);
        
        $basket->addProduct($basketProduct);

        $this->expectExceptionMessage('The selected product is not in the basket.');
        $pickingService->removeProductFromBasket($basket, $otherProduct);
    }
}